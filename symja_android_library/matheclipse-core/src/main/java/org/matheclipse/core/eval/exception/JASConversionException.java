package org.matheclipse.core.eval.exception;


public class JASConversionException extends Throwable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1572094432627031023L;

	public JASConversionException() {
		super("JAS conversion error");
	}
}
