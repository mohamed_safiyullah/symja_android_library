=============================================
  Symja Library - Java Symbolic Math System
=============================================

Features:
* arbitrary precision integers, rational and complex numbers
* differentiation, integration
* polynomials
* pattern matching
* linear algebra

The Symja library uses the Apache Commons Mathematics Library:
    http://commons.apache.org/math/
and the JAS - Java Algebra System:
    http://krum.rz.uni-mannheim.de/jas/ 
    http://code.google.com/p/java-algebra-system/
	
Symja is the underlying library for the SymjaDroid Android app:
    https://bitbucket.org/axelclk/symjadroid